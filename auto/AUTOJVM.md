# Auto JVM

This pipeline allows you to build a JVM application and deploy it.

Supports:
* Maven
* Gradle

It automates the following stages:
1. Testing
1. Code quality scans
1. Building a container image
1. Deploying to Kubernetes

Deployment uses the standard AutoDevOps deploy variables, however it has been configured to use Helm 3 instead of 2.

## Requirements

* Valid Maven/Gradle config files (see [Gradle-Test](../test/Gradle-Test.gitlab-ci.yml) and [Maven-Test](../test/Maven-Test.gitlab-ci.yml) for examples)
* An executable JAR must be built to the ./build/libs directory for Gradle, or the ./target directory for Maven

## Configuration

`AUTO_JVM_GRADLE_DOCKERFILE` - customise the URI which the Gradle `Dockerfile` is downloaded from.

`AUTO_JVM_MAVEN_DOCKERFILE` - customise the URI which the Maven `Dockerfile` is downloaded from.

`AUTO_JVM_DOCKERIGNORE` - customise the URI which the `.dockerignore` is downloaded from.

`PREPARE_DISABLED` - if present will skip downloading `Dockerfile`/`.dockerignore`. *Note: you will need to provide your own*

**Downloaded files must be publicly available.** If this is unacceptable, you can set `PREPARE_DISABLED` and create a custom prepare stage.
This stage will include your custom Dockerfile.

**Example**

```yaml
variables:
    PREPARE_DISABLED: 'true'

prepare_custom_resources:
  stage: .pre
  image:
    name: registry.example.org/my-custom-dockerfile-image
    entrypoint: [""]
  script:
    - echo "Nothing to do here, Dockerfile is baked into this image!"
  artifacts:
    paths:
      - Dockerfile
      - .dockerignore
    expire_in: 1 day
  rules:
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null' # same as only branches, tags
      when: never
```

## Example usage

```yaml
# auto devops variables
variables:
  PERFORMANCE_DISABLED: 'true' # ee feature

include:
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/Auto-JVM.gitlab-ci.yml'
```