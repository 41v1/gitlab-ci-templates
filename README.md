# AutoDevOps

This repo contains a number of GitLab CI templates which serve as extended configuration of the base GitLab [AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/).

### Design methodologies

* Do not allow Docker-In-Docker (`dind`) due to security concerns
* Targeting GitLab CE, however EE features aren't frowned against
* Use Alpine or Distro-less images where possible to minimise the security risks
* Target latest Kubernetes APIs
* [RULES.md](./RULES.md)

## Getting started

To get started, pick a method of deployment:

* I want to deploy my [web page](auto/AUTOUI.md)
* I want to [test my app](test/README.md)
* I want to deploy my [Maven app](auto/AUTOJVM.md)
* I want to deploy my [Gradle app](auto/AUTOJVM.md)
* I want to deploy my [GoLang app](auto/AUTOGOMOD.md)
