# Rules

[Rules](https://docs.gitlab.com/ee/ci/yaml/#rules) are a feature provided by GitLab CI which allows a user to include or exclude jobs from a pipeline.

Due to their nature, `rules` can be very long and cryptic.

## Standard rules

The goal of the AutoDevOps ruleset is to provide the following:
* Jobs run on branches/tags when a developer pushes new code
* Jobs run when an MR is created or updated allowing for MR-specific behaviour
* Pipelines are not duplicated when an MR is open

```yaml
echo:
  script: echo "Hello, World!"
  rules:
    - if: '$CI_OPEN_MERGE_REQUESTS != null && $CI_PIPELINE_SOURCE == "push"'
      when: never
    - if: '$CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: never
    - when: on_success
```
As you can tell, this is confusing and hard to read.

### Why not reverse it?

These rules are exclusionary to match the GitLab AutoDevOps templates that this project is based on.
Any changes that GitLab makes can be easily applied to this project without requiring the rules to be rewritten.

### Explanation

These rules do the following:

1. If an MR is open, and the current pipeline is a *branch* or *tag* pipeline, **do not run**.
1. If an MR is **not** open, and the current pipeline is *not* a *branch* or *tag* pipeline, **do not run**.

This effectively evaluates to the jobs only running if:

* The pipeline is either an MR pipeline *or* a branch/tag pipeline, but not both.